﻿using ArticleTableau.Structures;
using System;
using System.Collections.Generic;

namespace ArticleTableau
{
    class Program
    {
        static void Main(string[] args)
        {
            var listArticle = new List<ArticleType>();
            listArticle.Add(new ArticleType("stp", 2, 2, Structures.Type.Alimentaire));
            listArticle.Add(new ArticleType("hey", 6, 9, Structures.Type.Alimentaire));
            listArticle.Add(new ArticleType("ho", 8,12, Structures.Type.Alimentaire));

            listArticle.ForEach(x =>
            {
                x.Afficher();
            });
        }
    }
}
